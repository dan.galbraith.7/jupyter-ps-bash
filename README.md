# Jupyter PowerShell, dotnet, and bash Container

This container is for writing and running Jupyter Notebooks. In addition to python, it also includes bash, and the dotnet interactive kernels for C#, F#, and PowerShell.

## Use
This container intentionally has write access to your `$HOME/Documents/work` folder. The container is meant to be disposable. Your work is not.

### Build
1. Clone the repo to your system with `git clone https://gitlab.com/dan.galbraith.7/jupyter-ps-bash.git`
2. Enter the repo with `cd jupyter-ps-bash`
3. Build the container with `docker build . --tag jupyter-ps-bash:latest`

### Run
```bash
mkdir -p ~/Documents/work

docker run -d \
    --name jupyter-ps-bash \
    -p 127.0.0.1:8888:8888 \
    -e JUPYTER_ENABLE_LAB=yes \
    -v ~/Documents/work:/home/jovyan/work \
    jupyter-ps-bash:latest
```
Finally, run `docker logs jupyter-ps-bash` to get the login token and navigate to [http://localhost:8888](http://localhost:8888) to get started.
