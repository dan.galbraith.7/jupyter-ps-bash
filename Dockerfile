FROM jupyter/base-notebook:ubuntu-20.04

USER 0
# Install PowerShell Prereqs
RUN apt-get update && apt-get -y upgrade && apt-get install -y wget apt-transport-https software-properties-common
RUN wget -q https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb && add-apt-repository universe && apt-get update

# Install .NET 5.0 SDK
RUN apt-get install -y dotnet-sdk-5.0

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

# Back to Jovyan
USER 1000

# Install the Dotnet Interactive Kernel (C#, Pwsh)
RUN dotnet tool install -g --add-source "https://pkgs.dev.azure.com/dnceng/public/_packaging/dotnet-tools/nuget/v3/index.json" Microsoft.dotnet-interactive

# Install the Jupyter Kernel
RUN .dotnet/tools/dotnet-interactive jupyter install

# Try installing Bash
RUN pip install bash_kernel && python -m bash_kernel.install

# Fix Broken $PATH

ENTRYPOINT [ "/entrypoint.sh" ]